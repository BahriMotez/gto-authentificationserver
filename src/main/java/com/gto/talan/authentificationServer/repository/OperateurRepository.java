package com.gto.talan.authentificationServer.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gto.talan.authentificationServer.model.Operateur;

@Repository
public interface OperateurRepository extends JpaRepository<Operateur, Long> {
    Optional<Operateur> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    List<Operateur> findByIsTeamManager(boolean isTeamManager);
    List<Operateur> findByEquipeIdAndIsTeamManager(Long equipeId ,boolean isTeamManager);
    List<Operateur> findByBureauIdAndIsTeamManager(Long equipeId ,boolean isTeamManager);
}
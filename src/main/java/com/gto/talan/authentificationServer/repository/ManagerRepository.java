package com.gto.talan.authentificationServer.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.gto.talan.authentificationServer.model.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {
    Optional<Manager> findByUsername(String username);
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
}
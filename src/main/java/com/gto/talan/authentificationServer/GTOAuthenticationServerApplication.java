package com.gto.talan.authentificationServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.gto.talan.authentificationServer.model.Administrateur;
import com.gto.talan.authentificationServer.model.Manager;
import com.gto.talan.authentificationServer.model.Operateur;
import com.gto.talan.authentificationServer.model.Role;
import com.gto.talan.authentificationServer.model.RoleName;
import com.gto.talan.authentificationServer.repository.RoleRepository;
import com.gto.talan.authentificationServer.repository.UserRepository;
import com.gto.talan.authentificationServer.security.jwt.JwtProvider;
import com.gto.talan.authentificationServer.service.AdministrateurService;
import com.gto.talan.authentificationServer.service.ManagerService;
import com.gto.talan.authentificationServer.service.OperateurService;

@SpringBootApplication
@EnableDiscoveryClient
public class GTOAuthenticationServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GTOAuthenticationServerApplication.class, args);
	}

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	AdministrateurService administrateurService;

	@Autowired
	ManagerService managerService;

	@Autowired
	OperateurService operateurService;
	
	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@Override
	public void run(String... args) throws Exception {

		/*roleRepository.save(new Role(RoleName.ROLE_ADMIN));
		roleRepository.save(new Role(RoleName.ROLE_CLIENT));
		roleRepository.save(new Role(RoleName.ROLE_MANAGER));
		roleRepository.save(new Role(RoleName.ROLE_OPERATEUR));
		roleRepository.save(new Role(RoleName.ROLE_TEAM_MANAGER));
		roleRepository.save(new Role(RoleName.ROLE_USER));

		Administrateur admin = new Administrateur("admin", "Admin", "Admi@gmail.com", encoder.encode("AdminAdmin"),1L);
		
		Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
		admin.getRoles().add(adminRole);

		administrateurService.save(admin);*/
		/*+-----------------------------------------------------------------+
		 *|	add some Managers                                               |
		 *+-----------------------------------------------------------------+ 
		 */
		/*Manager m1 = new Manager("Amir", "M_Amir", "amir@gmail.com", encoder.encode("M_AmirM_Amir"),1L);
		Manager m2 = new Manager("Ahmed", "M_Ahmed", "Ahmed1@gmail.com", encoder.encode("M_AhmedM_Ahmed"),2L);
		Manager m3 = new Manager("Mounir", "M_Mounir", "mounir@gmail.com", encoder.encode("M_MounirM_Mounir"),3L);
		Role managerRole = roleRepository.findByName(RoleName.ROLE_MANAGER)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
		m1.getRoles().add(managerRole);
		m2.getRoles().add(managerRole);
		m3.getRoles().add(managerRole);
		managerService.save(m1);
		managerService.save(m2);
		managerService.save(m3);*/
		
		/*+-----------------------------------------------------------------+
		 *|	add some team Managers                                          |
		 *+-----------------------------------------------------------------+ 
		 */
		/*Operateur tm1 = new Operateur("ahmed", "benAziza12", "ahmed@gmail.com", encoder.encode("benAziza12benAziza12"), true,1L,1L,m1);
		Operateur tm2 = new Operateur("ali", "Ali123", "Ali123@gmail.com", encoder.encode("Ali123Ali123"), true,1L,2L,m2);
		Operateur tm3 = new Operateur("mohamed", "Mohamed123", "Mohamed123@gmail.com", encoder.encode("Mohamed123Mohamed123"), true,3L,3L,m3);
		Role teamManagerRole = roleRepository.findByName(RoleName.ROLE_TEAM_MANAGER)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
		tm1.getRoles().add(teamManagerRole);
		tm2.getRoles().add(teamManagerRole);
		tm3.getRoles().add(teamManagerRole);
		operateurService.save(tm1);
		operateurService.save(tm2);
		operateurService.save(tm3);*/
		
		/*+-----------------------------------------------------------------+
		 *|	add some Operateur                                              |
		 *+-----------------------------------------------------------------+ 
		 */
		/*Operateur op1 = new Operateur("salahsalah", "salahsalah", "salahsalah@gmail.com", encoder.encode("salahsalahsalahsalah"), false,tm1,1L,1L);
		Operateur op2 = new Operateur("ismail", "ismailismail", "ismailismail@gmail.com", encoder.encode("ismailismail"), false,tm1,1L,1L);
		Operateur op3 = new Operateur("monji", "monjibk", "monjibk@gmail.com", encoder.encode("monjibkmonjibk"), false,tm1,1L,1L);
		Operateur op4 = new Operateur("slim", "salimswissi", "salimswissi@gmail.com", encoder.encode("salimswissisalimswissi"), false,tm2,2L,2L);
		Operateur op5 = new Operateur("nour", "NourLight", "NourLight@gmail.com", encoder.encode("NourLightNourLight"), false,tm2,2L,2L);
		Operateur op6 = new Operateur("islem", "IslemISLEM", "IslemISLEM@gmail.com", encoder.encode("IslemISLEMIslemISLEM"), false,tm2,2L,2L);
		Operateur op7 = new Operateur("hela", "HELA_ZA", "HELA_ZA@gmail.com", encoder.encode("HELA_ZAHELA_ZA"), false,tm3,3L,3L);
		Operateur op8 = new Operateur("samir", "SamirTaib", "SamirTaib@gmail.com", encoder.encode("SamirTaibSamirTaib"), false,tm3,3L,3L);
		Operateur op9 = new Operateur("montassar", "MontaMonta", "MontaMonta@gmail.com", encoder.encode("MontaMontaMontaMonta"), false,tm3,3L,3L);
		Operateur op10 = new Operateur("samir", "Samirsqmir", "Samirsqmir@gmail.com", encoder.encode("MontatqssrMontatqssr"), false,tm3,3L,3L);
		Operateur op11 = new Operateur("montassar", "Montatqssr", "Montatqssr@gmail.com", encoder.encode("MontatqssrMontatqssr"), false,tm3,3L,3L);
		
		
		Role operateurRole = roleRepository.findByName(RoleName.ROLE_OPERATEUR)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
		
		op1.getRoles().add(operateurRole);
		op2.getRoles().add(operateurRole);
		op3.getRoles().add(operateurRole);
		op4.getRoles().add(operateurRole);
		op5.getRoles().add(operateurRole);
		op6.getRoles().add(operateurRole);
		op7.getRoles().add(operateurRole);
		op8.getRoles().add(operateurRole);
		op9.getRoles().add(operateurRole);
		op10.getRoles().add(operateurRole);
		op11.getRoles().add(operateurRole);
		
		operateurService.save(op1);
		operateurService.save(op2);
		operateurService.save(op3);
		operateurService.save(op4);
		operateurService.save(op5);
		operateurService.save(op6);
		operateurService.save(op7);
		operateurService.save(op8);
		operateurService.save(op9);
		operateurService.save(op10);
		operateurService.save(op11);*/
	}
}

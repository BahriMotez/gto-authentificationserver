package com.gto.talan.authentificationServer.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api/Employee")
public class EmployeeRestAPIs {
	
	@GetMapping("/Manager")
	@PreAuthorize("hasRole('MANAGER')")
	public String managerAccess() {
		return ">>> manager Contents!";
	}
	
	@GetMapping("/Operateur")
	@PreAuthorize("hasRole('OPERATEUR')")
	public String OperateurAccess() {
		return ">>> operateur Board";
	}
	
	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return ">>> Admin Contents";
	}
	@GetMapping("/TeamManager")
	@PreAuthorize("hasRole('TEAM_MANAGER')")
	public String teamManagerAccess() {
		return ">>> team manager Contents";
	}
	@GetMapping("/Client")
	@PreAuthorize("hasRole('CLIENT')")
	public String clientAccess() {
		return ">>> Client Contents";
	}
}
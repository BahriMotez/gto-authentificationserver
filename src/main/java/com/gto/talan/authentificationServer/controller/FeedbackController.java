package com.gto.talan.authentificationServer.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gto.talan.authentificationServer.model.Feedback;
import com.sun.mail.iap.Response;

import java.util.Properties;

import javax.mail.MessagingException;

import javax.mail.internet.MimeMessage;
import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
@RequestMapping("/feedback")
@CrossOrigin
public class FeedbackController {


    private static final Logger LOGGER = LoggerFactory.getLogger(FeedbackController.class);

    @Autowired
    private JavaMailSender javaMailSender;
    
    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = "application/json")
    @PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('CLIENT')")
	public Response sendEmail(@RequestBody Feedback emailModel) {

        LOGGER.info("Sending email");



        MimeMessage mail = javaMailSender.createMimeMessage();

        try {

            MimeMessageHelper helper = new MimeMessageHelper(mail, true);

            helper.setTo(emailModel.getEmailTo());

            //helper.setCc("gtotesttalan@gmail.com");

            //helper.setFrom(emailModel.getEmailFrom());

            helper.setSubject(emailModel.getSubject());

            helper.setText(emailModel.getMessage());

        } catch (MessagingException e) {

            LOGGER.error("Failed to send email: " + emailModel.toString(), e);
        } finally {}

        javaMailSender.send(mail);
        return null;




    }

}
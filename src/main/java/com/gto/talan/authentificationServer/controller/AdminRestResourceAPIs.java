package com.gto.talan.authentificationServer.controller;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gto.talan.authentificationServer.message.request.SignUpEmpForm;
import com.gto.talan.authentificationServer.message.request.SignUpForm;
import com.gto.talan.authentificationServer.message.response.ResponseMessage;
import com.gto.talan.authentificationServer.model.Administrateur;
import com.gto.talan.authentificationServer.model.Client;
import com.gto.talan.authentificationServer.model.Manager;
import com.gto.talan.authentificationServer.model.Operateur;
import com.gto.talan.authentificationServer.model.Role;
import com.gto.talan.authentificationServer.model.RoleName;
import com.gto.talan.authentificationServer.model.User;
import com.gto.talan.authentificationServer.repository.RoleRepository;
import com.gto.talan.authentificationServer.repository.UserRepository;
import com.gto.talan.authentificationServer.security.jwt.JwtProvider;
import com.gto.talan.authentificationServer.service.AdministrateurService;
import com.gto.talan.authentificationServer.service.ClientService;
import com.gto.talan.authentificationServer.service.ManagerService;
import com.gto.talan.authentificationServer.service.OperateurService;

@RestController
@RequestMapping("/admin")
public class AdminRestResourceAPIs {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	ManagerService managerService;

	@Autowired
	AdministrateurService administrateurService;

	@Autowired
	OperateurService operateurService;

	@Autowired
	ClientService clientService;

	/*
	 * +---------------------------------------------------------------------------+
	 * | input : SignUpForm Object | | Processing : verify the information provided
	 * by the user in the | | request, there is two contraints that we should verify
	 * before we can do | | anything the unicity of the userName and the Email then
	 * we create a new | | user and add the Roles to that user, and at the end we
	 * can add him to the | | database. | | output : JWt Response with a msg : User
	 * registered successfully! | |
	 * +---------------------------------------------------------------------------+
	 */
	@PostMapping("/signupEmployee")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> registerEmployee(@Valid @RequestBody SignUpEmpForm signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		User user;
		switch (signUpRequest.getType().toLowerCase()) {
		case "admin":
			user = new Administrateur(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
					encoder.encode(signUpRequest.getPassword()), signUpRequest.getIdBureau());
			break;
		case "manager":
			user = new Manager(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
					encoder.encode(signUpRequest.getPassword()), signUpRequest.getIdBureau());
			break;
		case "team_manager":
			Manager m = managerService.findById(signUpRequest.getIdmanager()).get();
			if (m == null) {
				return new ResponseEntity<>(
						new ResponseMessage(
								"Fail -> we can't find the team manager plz make sure u pick the right one!"),
						HttpStatus.BAD_REQUEST);
			} else {
				user = new Operateur(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
						encoder.encode(signUpRequest.getPassword()), true, signUpRequest.getIdBureau(),
						signUpRequest.getIdEquipe(), m);
			}
			break;
		case "operateur":

			Operateur r = operateurService.findById(signUpRequest.getIdTeamManager()).get();

			if (r == null) {
				return new ResponseEntity<>(
						new ResponseMessage("Fail -> we can't find the operator plz make sure u pick the right one!"),
						HttpStatus.BAD_REQUEST);
			} else {
				user = new Operateur(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
						encoder.encode(signUpRequest.getPassword()), false, r, signUpRequest.getIdBureau(),
						signUpRequest.getIdEquipe());
			}
			break;
		default:
			user = new Client(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
					encoder.encode(signUpRequest.getPassword()), "Client");
			break;
		}

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Admin Role not find."));
				roles.add(adminRole);

				break;
			case "manager":
				Role managerRole = roleRepository.findByName(RoleName.ROLE_MANAGER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Manager Role not find."));
				roles.add(managerRole);

				break;
			case "team_manager":
				Role team_managerRole = roleRepository.findByName(RoleName.ROLE_TEAM_MANAGER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Team Manager Role not find."));
				roles.add(team_managerRole);

				break;
			case "operateur":
				Role operateurRole = roleRepository.findByName(RoleName.ROLE_OPERATEUR)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Operator Role not find."));
				roles.add(operateurRole);

				break;
			case "client":
				Role clientRole = roleRepository.findByName(RoleName.ROLE_CLIENT)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Client Role not find."));
				roles.add(clientRole);

				break;
			default:
				Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});
		user.setRoles(roles);
		user.setFirstLogin(true);
		userRepository.save(user);
		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}

	@PostMapping("/updateEmployee/{employeeId}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<?> updateEmployee(@PathVariable Long employeeId,
			@Valid @RequestBody SignUpEmpForm signUpRequest) {

		// Creating user's account
		User user;
		switch (signUpRequest.getType().toLowerCase()) {
		case "admin":
			user = new Administrateur(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
					signUpRequest.getPassword(), signUpRequest.getIdBureau());
			user.setId(employeeId);
			break;
		case "manager":
			user = new Manager(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
					signUpRequest.getPassword(), signUpRequest.getIdBureau());
			user.setId(employeeId);
			break;
		case "team_manager":
			Manager m = managerService.findById(signUpRequest.getIdmanager()).get();
			if (m == null) {
				return new ResponseEntity<>(
						new ResponseMessage(
								"Fail -> we can't find the team manager plz make sure u pick the right one!"),
						HttpStatus.BAD_REQUEST);
			} else {
				user = new Operateur(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
						signUpRequest.getPassword(), true, signUpRequest.getIdBureau(), signUpRequest.getIdEquipe(), m);
				user.setId(employeeId);
			}
			break;
		case "operateur":

			Operateur r = operateurService.findById(signUpRequest.getIdTeamManager()).get();

			if (r == null) {
				return new ResponseEntity<>(
						new ResponseMessage("Fail -> we can't find the operator plz make sure u pick the right one!"),
						HttpStatus.BAD_REQUEST);
			} else {
				user = new Operateur(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
						signUpRequest.getPassword(), false, r, signUpRequest.getIdBureau(),
						signUpRequest.getIdEquipe());
				user.setId(employeeId);
			}
			break;
		default:
			user = new Client(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
					signUpRequest.getPassword(), "Client");
			user.setId(employeeId);
			break;
		}

		Set<String> strRoles = signUpRequest.getRole();
		Set<Role> roles = new HashSet<>();

		strRoles.forEach(role -> {
			switch (role) {
			case "admin":
				Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Admin Role not find."));
				roles.add(adminRole);
				break;
			case "manager":
				Role managerRole = roleRepository.findByName(RoleName.ROLE_MANAGER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Manager Role not find."));
				roles.add(managerRole);

				break;
			case "team_manager":
				Role team_managerRole = roleRepository.findByName(RoleName.ROLE_TEAM_MANAGER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Team Manager Role not find."));
				roles.add(team_managerRole);
				break;
			case "operateur":
				Role operateurRole = roleRepository.findByName(RoleName.ROLE_OPERATEUR)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Operator Role not find."));
				roles.add(operateurRole);
				break;
			case "client":
				Role clientRole = roleRepository.findByName(RoleName.ROLE_CLIENT)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Client Role not find."));
				roles.add(clientRole);
				break;
			default:
				Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
						.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
				roles.add(userRole);
			}
		});
		user.setRoles(roles);

		userRepository.save(user);
		return new ResponseEntity<>(new ResponseMessage("User updated successfully!"), HttpStatus.OK);
	}

	@GetMapping("/getAllManager")
	@PreAuthorize("hasRole('ADMIN')")
	List<Manager> getAllManager() {
		return managerService.findAll();
	}

	@PostMapping("/updatePasswordEmployee/{idUser}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('CLIENT')")
	User updatePasswordEmployee(@PathVariable Long idUser,@RequestParam("password") String password) {
		User user = userRepository.findById(idUser).get();
		user.setFirstLogin(false);
		user.setPassword(encoder.encode(password));
		userRepository.save(user);
		return userRepository.findById(idUser).get();
	}
	
	@PostMapping("/updatePasswordEmployeeInsideProfile/{idUser}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('CLIENT')")
	User updatePasswordEmployeewithOld(@PathVariable Long idUser,@RequestParam("oldPassword") String oldpassword
			,@RequestParam("newPassword") String newpassword) {
		User user = userRepository.findById(idUser).get();
		user.setFirstLogin(false);
		if(encoder.matches(oldpassword, user.getPassword())) {
			user.setPassword(encoder.encode(newpassword));
			userRepository.save(user);
			return userRepository.findById(idUser).get();
		} else {
			return null;
		}
		
	}
	
	@PostMapping("/updateEmployeeInfo/{idUser}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR')")
	User updateEmployeeInfo(@PathVariable Long idUser,@RequestBody User liste) {
		User user = userRepository.findById(idUser).get();
		
		user.setFirstLogin(false);
		user.setAdresse(liste.getAdresse());
		user.setVille(liste.getVille());
		user.setGouvernorat(liste.getGouvernorat());
		user.setName(liste.getName());
		user.setDateNaissance(liste.getDateNaissance());
		user.setTel(liste.getTel());
		
		userRepository.save(user);
		return userRepository.findById(idUser).get();
	}
	
	@PostMapping("/updateClientInfo/{idClient}")
	@PreAuthorize("hasRole('CLIENT')")
	Client updateClientInfo(@PathVariable Long idClient,@RequestBody Client liste) {
		Client user = clientService.findById(idClient).get();
		
		user.setFirstLogin(false);
		user.setAdresse(liste.getAdresse());
		user.setVille(liste.getVille());
		user.setGouvernorat(liste.getGouvernorat());
		user.setName(liste.getName());
		user.setDateNaissance(liste.getDateNaissance());
		user.setTel(liste.getTel());
		user.setProfession(liste.getProfession());
		user.setEtatCivil(liste.getEtatCivil());
		clientService.save(user);
		return clientService.findById(idClient).get();
	}
	
	@GetMapping("/getOperateurByBureau/{idBureau}/{isTeamManager}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('CLIENT')")
	List<Operateur> getOperateurByBureau(@PathVariable Long idBureau, @PathVariable boolean isTeamManager) {
		return operateurService.findByBureauIdAndIsTeamManager(idBureau, isTeamManager);
	}

	@GetMapping("/getAllAdministrateur")
	@PreAuthorize("hasRole('ADMIN')")
	List<Administrateur> getAllAdministrateur() {
		return administrateurService.findAll();
	}

	@GetMapping("/getEmployee/{username}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('CLIENT')")
	User getEmployee(@PathVariable String username) {
		if (userRepository.existsByUsername(username)) {
			return userRepository.findByUsername(username).get();
		}
		return null;
	}

	@GetMapping("/getAllClient")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('CLIENT')")
	List<Client> getAllClient() {
		return clientService.findAll();
	}

	@PostMapping("/getClient")
	@PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT')")
	Client getClient(@RequestParam("username") String username) {
		return clientService.findByUsername(username)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: UserName Not exist."));
	}

	@GetMapping("/getClientById/{clientId}")
	@PreAuthorize("hasRole('ADMIN')  or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('TEAM_MANAGER') or hasRole('CLIENT')")
	Client getClientById(@PathVariable Long clientId) {
		return clientService.findById(clientId)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: Id Not exist."));
	}

	@PostMapping("/getTeamManager")
	@PreAuthorize("hasRole('ADMIN') or hasRole('TEAM_MANAGER') or hasRole('MANAGER')")
	Operateur getTeamManager(@RequestParam("username") String username) {
		return operateurService.findByUsername(username)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: UserName Not exist."));
	}

	@PostMapping("/getManager")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER')")
	Manager getManager(@RequestParam("username") String username) {
		return managerService.findByUsername(username)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: UserName Not exist."));
	}

	@PostMapping("/getAdmin")
	@PreAuthorize("hasRole('ADMIN')")
	Administrateur getAdmin(@RequestParam("username") String username) {
		return administrateurService.findByUsername(username)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: UserName Not exist."));
	}

	@PostMapping("/getOperateur")
	@PreAuthorize("hasRole('ADMIN') or hasRole('OPERATEUR')")
	Operateur getOperateur(@RequestParam("username") String username) {
		return operateurService.findByUsername(username)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: UserName Not exist."));
	}

	@GetMapping("/getAllOperateur")
	@PreAuthorize("hasRole('ADMIN')")
	List<Operateur> getAllOperateur() {
		return operateurService.findAllOperateurs();
	}

	@GetMapping("/getAllTeamManager")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER')")
	List<Operateur> getAllTeamManager() {
		return operateurService.findAllTeamManagers();
	}

	@GetMapping("/getAllRoles")
	@PreAuthorize("hasRole('ADMIN')")
	List<Role> getAllRoles() {
		return roleRepository.findAll();
	}

	@GetMapping("/getOperateurById/{operateurID}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('TEAM_MANAGER') or hasRole('CLIENT')")
	Operateur getOperateurById(@PathVariable Long operateurID) {
		return operateurService.findById(operateurID)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: id Operateur Not exist."));
	}

	@GetMapping("/getTeamManagerById/{teamManagerID}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('OPERATEUR') or hasRole('TEAM_MANAGER') or hasRole('CLIENT')")
	Operateur getTeamManagerById(@PathVariable Long teamManagerID) {
		return operateurService.findById(teamManagerID)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: id TeamManager Not exist."));
	}

	@GetMapping("/getManagerById/{managerID}")
	@PreAuthorize("hasRole('ADMIN')")
	Manager getManagerById(@PathVariable Long managerID) {
		return managerService.findById(managerID)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: id Manager Not exist."));
	}

	@GetMapping("/getAdministrateurById/{adminID}")
	@PreAuthorize("hasRole('ADMIN')")
	Administrateur getAdministrateurById(@PathVariable Long adminID) {
		return administrateurService.findById(adminID)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: id Administrateur Not exist."));
	}

	@GetMapping("/getOperateurByEquipe/{idequipe}")
	@PreAuthorize("hasRole('ADMIN') or hasRole('MANAGER') or hasRole('TEAM_MANAGER') or hasRole('OPERATEUR')")
	List<Operateur> getOperateurByEquipe(@PathVariable Long idequipe) {
		return operateurService.findByEquipeIdAndIsTeamManager(idequipe, false);
	}

	@GetMapping("/deleteAdministrateur/{adminId}")
	@PreAuthorize("hasRole('ADMIN')")
	ResponseEntity<?> deleteAdministrateur(@PathVariable Long adminId) {
		try {
			administrateurService.deleteById(adminId);
			return new ResponseEntity<>(new ResponseMessage("Success -> Administrateur successfully deleted!"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> internal error!"), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/deleteManager/{managerId}/{replaceManagerId}")
	@PreAuthorize("hasRole('ADMIN')")
	ResponseEntity<?> deleteManager(@PathVariable Long managerId, @PathVariable Long replaceManagerId) {
		try {
			Manager opManager = managerService.findById(replaceManagerId).get();
			if (opManager == null) {
				return new ResponseEntity<>(new ResponseMessage("Fail -> Replaced Manager is incorrect!"),
						HttpStatus.BAD_REQUEST);
			}
			List<Operateur> lst = operateurService.findAllTeamManagers();
			Iterator<Operateur> iter = lst.iterator();
			while (iter.hasNext()) {
				Operateur r = iter.next();
				if (r.getManager().getId() == managerId) {
					r.setManager(opManager);
				}
				operateurService.save(r);
			}
			managerService.deleteById(managerId);
			return new ResponseEntity<>(new ResponseMessage("Success -> Manager successfully deleted!"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> internal error!"), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/deleteTeamManager/{teamManagerId}/{replaceTeamManagerId}")
	@PreAuthorize("hasRole('ADMIN')")
	ResponseEntity<?> deleteTeamManager(@PathVariable Long teamManagerId, @PathVariable Long replaceTeamManagerId) {
		try {
			Operateur opTeamManager = operateurService.findById(replaceTeamManagerId).get();
			if (opTeamManager == null) {
				return new ResponseEntity<>(new ResponseMessage("Fail -> Replaced Team Manager is incorrect!"),
						HttpStatus.BAD_REQUEST);
			}
			List<Operateur> lst = operateurService.findAllOperateurs();
			Iterator<Operateur> iter = lst.iterator();
			while (iter.hasNext()) {
				Operateur r = iter.next();
				if (r.getTeam_manager().getId() == teamManagerId) {
					r.setTeam_manager(opTeamManager);
				}
				operateurService.save(r);
			}
			operateurService.deleteById(teamManagerId);
			return new ResponseEntity<>(new ResponseMessage("Success -> Team Manager successfully deleted!"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> internal error!"), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/deleteOperateur/{operateurId}")
	@PreAuthorize("hasRole('ADMIN')")
	ResponseEntity<?> deleteOperateur(@PathVariable Long operateurId) {
		try {
			operateurService.deleteById(operateurId);
			return new ResponseEntity<>(new ResponseMessage("Success -> Operator successfully deleted!"),
					HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> internal error!"), HttpStatus.BAD_REQUEST);
		}
	}

}

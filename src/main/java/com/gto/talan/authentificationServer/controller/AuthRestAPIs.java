package com.gto.talan.authentificationServer.controller;

import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gto.talan.authentificationServer.message.request.LoginForm;
import com.gto.talan.authentificationServer.message.request.SignUpForm;
import com.gto.talan.authentificationServer.message.response.JwtResponse;
import com.gto.talan.authentificationServer.message.response.ResponseMessage;
import com.gto.talan.authentificationServer.model.Client;
import com.gto.talan.authentificationServer.model.Role;
import com.gto.talan.authentificationServer.model.RoleName;
import com.gto.talan.authentificationServer.model.User;
import com.gto.talan.authentificationServer.repository.RoleRepository;
import com.gto.talan.authentificationServer.repository.UserRepository;
import com.gto.talan.authentificationServer.security.jwt.JwtProvider;


@RestController
@RequestMapping("/auth")
public class AuthRestAPIs {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtProvider jwtProvider;

	/*
     * +---------------------------------------------------------------------------+
     * | input : LoginForm Object                                                  |
     * | Processing :  verify the information provided by the user in the previous |
     * | request, then if every thing is okay we move on to generate a token       |
     * | and make a responde to the client                                         |	
     * | output : JWt Response with the username and accesToken and the authorities|                                                 |
     * +---------------------------------------------------------------------------+
     */
	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
		

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = jwtProvider.generateJwtToken(authentication);
		UserDetails userDetails = (UserDetails) authentication.getPrincipal();
		
		return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
	}

	/*
     * +---------------------------------------------------------------------------+
     * | input : SignUpForm Object                                                 |
     * | Processing :  verify the information provided by the user in the          |
     * | request, there is two contraints that we should verify before we can do   |
     * | anything the unicity of the userName and the Email then we create a new   |
     * | client, and at the end we can add him to the database.	                   |                                                               |
     * | output : JWt Response with a msg : User registered successfully!          |                                                 |
     * +---------------------------------------------------------------------------+
     */
	@PostMapping("/signupClient")
	public ResponseEntity<?> registerClient(@Valid @RequestBody SignUpForm signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Username is already taken!"),
					HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity<>(new ResponseMessage("Fail -> Email is already in use!"),
					HttpStatus.BAD_REQUEST);
		}

		// Creating user's account
		Client client = new Client(signUpRequest.getName(), signUpRequest.getUsername(), signUpRequest.getEmail(),
				encoder.encode(signUpRequest.getPassword()),"Client");

		Role clientRole = roleRepository.findByName(RoleName.ROLE_CLIENT)
				.orElseThrow(() -> new RuntimeException("Fail! -> Cause: User Role not find."));
		
		client.getRoles().add(clientRole);

		userRepository.save(client);
		return new ResponseEntity<>(new ResponseMessage("User registered successfully!"), HttpStatus.OK);
	}
}
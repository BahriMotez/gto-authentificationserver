package com.gto.talan.authentificationServer.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Client")
@PrimaryKeyJoinColumn(name = "id")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Client extends User{
	@Column(name = "client")
	private String client;
	
	@Column(name = "etat_civil")
	private String etatCivil;
	
	@Column(name = "profession")
	private String profession;

	@Override
	public String toString() {
		return "Client [operateur=" + client + ", toString()=" + super.toString() + "]";
	}

	public Client(String name, String username, String email, String password, String client) {
		super(name, username, email, password);
		this.client = client;
	}
	
}

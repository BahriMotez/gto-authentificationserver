package com.gto.talan.authentificationServer.model;

public enum  RoleName {
	ROLE_USER,
	ROLE_CLIENT,
	ROLE_ADMIN,
	ROLE_MANAGER,
	ROLE_TEAM_MANAGER,
	ROLE_OPERATEUR
}
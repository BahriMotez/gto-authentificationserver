package com.gto.talan.authentificationServer.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class Feedback {

    @NotNull
    private String name;


    @NotNull
    private String subject;

    @NotNull
    @Email
    private String emailTo;

    @Email
    private String emailcc;

    @NotNull
    @Min(10)
    private String message;

}
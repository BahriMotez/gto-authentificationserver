package com.gto.talan.authentificationServer.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Administrateur")
@PrimaryKeyJoinColumn(name = "id")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Administrateur extends User{

	@Column(name = "bureauId")
	private Long bureauId;
	
	public Administrateur(String name, String username, String email, String password,Long bureauId) {
		super(name, username, email, password);
		this.bureauId = bureauId;
	}
	
	
}

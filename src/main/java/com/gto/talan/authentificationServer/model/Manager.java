package com.gto.talan.authentificationServer.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Manager")
@PrimaryKeyJoinColumn(name = "id")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Manager extends User{
	@OneToMany(mappedBy="manager")
    @JsonIgnore
	private Collection<Operateur> team_managers;
	
	@Column(name = "bureauId")
	private Long bureauId;
	
	
	public Manager(String name, String username, String email, String password,Long bureauId) {
		super(name, username, email, password);
		this.bureauId = bureauId;
	}

	
	
}

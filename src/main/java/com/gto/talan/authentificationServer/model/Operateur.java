package com.gto.talan.authentificationServer.model;

import java.util.Collection;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Operateur")
@PrimaryKeyJoinColumn(name = "id")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Operateur extends User{
	@Column(name = "is_Team_Manager")
	private boolean isTeamManager;

	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "team_manager_id")
	private Operateur team_manager;
	
	@OneToMany(mappedBy="team_manager")
    @JsonIgnore
	private Collection<Operateur> operateurs;
	
	@ManyToOne(cascade = { CascadeType.MERGE })
	@JoinColumn(name = "manager_id")
	private Manager manager;
	
	@Column(name = "bureauId")
	private Long bureauId;
	
	@Column(name = "equipeId")
	private Long equipeId;
	
	public Operateur(String name, String username, String email, String password,
			boolean isTeamManager,Long bureauId,Long equipeId,Manager manager) {
		super(name, username, email, password);
		this.isTeamManager = isTeamManager;
		this.bureauId = bureauId;
		this.equipeId = equipeId;
		this.manager = manager;
	}
	public Operateur(String name, String username, String email, String password,
			boolean isTeamManager,Operateur team_manager,Long bureauId,Long equipeId) {
		super(name, username, email, password);
		this.isTeamManager = isTeamManager;
		this.team_manager = team_manager;
		this.bureauId = bureauId;
		this.equipeId = equipeId;
	}

	
	
}

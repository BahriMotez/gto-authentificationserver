package com.gto.talan.authentificationServer.service;

import java.util.List;
import java.util.Optional;

import com.gto.talan.authentificationServer.model.Administrateur;




public interface AdministrateurService {
	
	List<Administrateur> findAll();
	
	Optional<Administrateur> findById(Long id);
	
	Optional<Administrateur> findByUsername(String username);
	
	void save(Administrateur liste);

	void deleteById(Long id);
	
}

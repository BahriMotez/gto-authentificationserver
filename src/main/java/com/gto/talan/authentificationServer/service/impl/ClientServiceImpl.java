package com.gto.talan.authentificationServer.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gto.talan.authentificationServer.model.Client;
import com.gto.talan.authentificationServer.repository.ClientRepository;
import com.gto.talan.authentificationServer.service.ClientService;


@Service
public class ClientServiceImpl implements ClientService{

	@Autowired
	private ClientRepository clientRepository;
	
	@Override
	public List<Client> findAll() {
		return clientRepository.findAll();
	}

	@Override
	public Optional<Client> findById(Long id) {
		return clientRepository.findById(id);
	}

	@Override
	public void save(Client liste) {
		clientRepository.save(liste);
	}

	@Override
	public void deleteById(Long id) {
		clientRepository.deleteById(id);
	}

	@Override
	public Optional<Client> findByUsername(String username) {
		return clientRepository.findByUsername(username);
	}

}

package com.gto.talan.authentificationServer.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gto.talan.authentificationServer.service.OperateurService;
import com.gto.talan.authentificationServer.model.Client;
import com.gto.talan.authentificationServer.model.Operateur;
import com.gto.talan.authentificationServer.repository.OperateurRepository;

@Service
public class OperateurServiceImpl implements OperateurService{

	@Autowired
	private OperateurRepository operateurRepository;
	

	@Override
	public Optional<Operateur> findById(Long id) {
		return operateurRepository.findById(id);
	}

	@Override
	public void save(Operateur liste) {
		operateurRepository.save(liste);
	}

	@Override
	public void deleteById(Long id) {
		operateurRepository.deleteById(id);
	}

	@Override
	public List<Operateur> findAllOperateurs() {
		return operateurRepository.findByIsTeamManager(false);
	}

	@Override
	public List<Operateur> findAllTeamManagers() {
		return operateurRepository.findByIsTeamManager(true);
	}

	@Override
	public Optional<Operateur> findByUsername(String username) {
		return operateurRepository.findByUsername(username);
	}

	@Override
	public List<Operateur> findByEquipeIdAndIsTeamManager(Long equipeId, boolean isTeamManager) {
		return operateurRepository.findByEquipeIdAndIsTeamManager(equipeId, isTeamManager);
	}

	@Override
	public List<Operateur> findByBureauIdAndIsTeamManager(Long equipeId, boolean isTeamManager) {
		return operateurRepository.findByBureauIdAndIsTeamManager(equipeId, isTeamManager);
	}
	
	
}

package com.gto.talan.authentificationServer.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gto.talan.authentificationServer.service.ManagerService;
import com.gto.talan.authentificationServer.model.Manager;
import com.gto.talan.authentificationServer.repository.ManagerRepository;

@Service
public class ManagerServiceImpl implements ManagerService{

	@Autowired
	private ManagerRepository managerRepository;
	
	@Override
	public List<Manager> findAll() {
		return managerRepository.findAll();
	}

	@Override
	public Optional<Manager> findById(Long id) {
		return managerRepository.findById(id);
	}

	@Override
	public void save(Manager liste) {
		managerRepository.save(liste);
	}

	@Override
	public void deleteById(Long id) {
		managerRepository.deleteById(id);
	}

	@Override
	public Optional<Manager> findByUsername(String username) {
		return managerRepository.findByUsername(username);
	}

}

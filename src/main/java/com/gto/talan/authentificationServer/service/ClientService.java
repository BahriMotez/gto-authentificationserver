package com.gto.talan.authentificationServer.service;

import java.util.List;
import java.util.Optional;

import com.gto.talan.authentificationServer.model.Client;



public interface ClientService {
	
	List<Client> findAll();
	
	Optional<Client> findById(Long id);
	
	Optional<Client> findByUsername(String username);
	
	void save(Client liste);

	void deleteById(Long id);
	
}

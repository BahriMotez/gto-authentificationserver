package com.gto.talan.authentificationServer.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gto.talan.authentificationServer.service.AdministrateurService;
import com.gto.talan.authentificationServer.model.Administrateur;
import com.gto.talan.authentificationServer.repository.AdminRepository;


@Service
public class AdministrateurServiceImpl implements AdministrateurService{

	@Autowired
	private AdminRepository administrateurRepository;
	
	@Override
	public List<Administrateur> findAll() {
		return administrateurRepository.findAll();
	}

	@Override
	public Optional<Administrateur> findById(Long id) {
		return administrateurRepository.findById(id);
	}

	@Override
	public void save(Administrateur liste) {
		administrateurRepository.save(liste);
	}

	@Override
	public void deleteById(Long id) {
		administrateurRepository.deleteById(id);
	}

	@Override
	public Optional<Administrateur> findByUsername(String username) {
		return administrateurRepository.findByUsername(username);
	}

}

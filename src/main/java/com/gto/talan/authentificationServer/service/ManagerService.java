package com.gto.talan.authentificationServer.service;

import java.util.List;
import java.util.Optional;

import com.gto.talan.authentificationServer.model.Manager;



public interface ManagerService {
	
	List<Manager> findAll();
	
	Optional<Manager> findById(Long id);
	
	Optional<Manager> findByUsername(String username);
	
	void save(Manager liste);

	void deleteById(Long id);
	
}

package com.gto.talan.authentificationServer.service;

import java.util.List;
import java.util.Optional;

import com.gto.talan.authentificationServer.model.Operateur;



public interface OperateurService {
	
	List<Operateur> findAllOperateurs();
	
	List<Operateur> findAllTeamManagers();
	
	List<Operateur> findByEquipeIdAndIsTeamManager(Long equipeId ,boolean isTeamManager);
	
	List<Operateur> findByBureauIdAndIsTeamManager(Long equipeId ,boolean isTeamManager);
	
	Optional<Operateur> findById(Long id);
	
	Optional<Operateur> findByUsername(String username);
	
	void save(Operateur liste);

	void deleteById(Long id);
	
}
